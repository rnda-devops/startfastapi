from fastapi import Path, Body
from pydantic import BaseModel, Field
from typing import List
from beanie import Document, init_beanie



class Title:
    STR = "the title of the comment"
    MAX_lENGTH = 32
    PATH_PARAM = Path(..., title= STR, max_length=MAX_lENGTH)


class Subject:
    STR = "the comment's subjects"


class Text:
    STR = "the display the comment"
    MAX_lENGTH = 455
    PATH_PARAM = Path(..., title= STR, max_length=MAX_lENGTH)



class TheCommentId:
    STR = " The Id of the comment"
    PATH_PARAM = Path(..., title=STR)

class PhotoId:
    STR = " The Id of the photo comment (display_photographer_name, id) "
    PATH_PARAM = Path(..., title=STR)


class Author:
    STR = " The name of the author "
    MAX_LENGTH = 52
    PATH_PARAM = Path(..., title= STR, max_length=MAX_LENGTH)

class Photographer:
    STR = " The name of the photographer"
    MAX_LENGTH = 52
    PATH_PARAM = Path(..., title= STR, max_length=MAX_LENGTH)

class NextComentId:
    STR = "The id of the next comment for a specifique user"


class CommentDesc(BaseModel):
    #comment_id  : str = Field(None, title = TheCommentId.STR)
    photo_id  : int = Field(None, title = PhotoId.STR)
    display_author_name  : str = Field(None, title = Author.STR, max_length= Author.MAX_LENGTH)
    display_photographer_name: str = Field(None, title= Photographer.STR, max_length=Photographer.MAX_LENGTH)
    text: str = Field(None, title = Text.STR)


# class  CommentIdDesc(BaseModel):
#     display_author_name : str = Field(None, title = Author.STR)
#     photo_id : str =  Field(None, title = PhotoId.STR)
#     next_comment_id : int = Field(None, title = NextComentId.STR)
    


class Comment(Document, CommentDesc):
    pass

#class CommentId(Document, CommentIdDesc):
#    pass

COMMENT_EXAMPLE = {
    # "comment_id" : "1",
    #"photo_id" : "0",
    "display_author_name" : "alpha",
    "text" : "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."
    
}



COMMENT_UPDATE_EXAMPLE = {
    
     "text" : "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."
    
 }

COMMENT_BODY = Body(..., example = COMMENT_EXAMPLE)
COMMENT_UPDATE_BODY = Body(..., example = COMMENT_UPDATE_EXAMPLE)

class CommentDigest(BaseModel):
    comment_id : object
    link : str 


class Comments(BaseModel):
    items: List[CommentDigest]
    has_more: bool 



