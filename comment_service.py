import uvicorn


from fastapi import Depends, FastAPI, HTTPException
from starlette.responses import Response
from fastapi.logger import logger

from starlette.requests import Request
from starlette.middleware.cors import CORSMiddleware
from pydantic import BaseModel,BaseSettings
from typing import List
import pymongo
import requests
from models import Title, Comment, CommentDesc, COMMENT_BODY, Comments, CommentDigest, PhotoId, Photographer, Author,TheCommentId,COMMENT_UPDATE_BODY

import logging
from beanie import Document, init_beanie
import asyncio, motor

import re

from comment_const import REQUEST_TIMEOUT

from config import Settings

settings = Settings()

photographer_service = 'http://' + settings.photographer_host + ':' + settings.photographer_port + '/'
photo_service = 'http://' + settings.photo_host + ':' + settings.photo_port + '/'


app = FastAPI(title = "Comment Service")

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:3000",
    "localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#FastAPI logging
gunicorn_logger = logging.getLogger('gunicorn.error')
logger.handlers = gunicorn_logger.handlers

@app.on_event("startup")
async def startup_event():
    conn = f"mongodb://"
    if settings.mongo_user:
        conn += f"{settings.mongo_user}:{settings.mongo_password}@"
    conn += f"{settings.mongo_host}:{settings.mongo_port}"
    conn += f"/{settings.database_name}?authSource={settings.auth_database_name}"
    client = motor.motor_asyncio.AsyncIOMotorClient(conn)
    await init_beanie(
        database=client[settings.database_name],
        document_models=[Comment]
        )

@app.post("/comment/{display_photographer_name}/{photo_id}", status_code = 201, description=" Create a comment for a specific photo ")
async def create_comment(response: Response, display_photographer_name: str = Photographer.PATH_PARAM , photo_id: int = PhotoId.PATH_PARAM , comment: CommentDesc = COMMENT_BODY):

        #   on verifie l"existance du user
        author = requests.get(photographer_service + 'photographer/' + comment.display_author_name,  timeout=REQUEST_TIMEOUT)
        if author.status_code == requests.codes.ok:
        #if True : 
        
            # on vérifie l'existance du photographe
            photographer = requests.get(photographer_service + 'photographer/' + display_photographer_name,  timeout=REQUEST_TIMEOUT)
            if photographer.status_code == requests.codes.ok:
            # if True :
  
                # on verifie l'existence de la photo
                photo = requests.get(photo_service + 'photo/' + display_photographer_name + "/" + str(photo_id) + "/attributes" ,  timeout=REQUEST_TIMEOUT)
                if photo.status_code == requests.codes.ok: 
                # if True :
                    # on enregistre le commentaire
                    
                    try:
                        # id = mongo_allocate_comment_id(comment.display_author_name, g_photo_id)
                        # check = await Comment.find_one(
                        #                     Comment.display_author_name == comment.display_author_name,
                        #                     Comment.photo_id == photo_id,
                        #                     Comment.display_photographer_name == display_photographer_name
                        #                     )

                        # if check is Non

                        myComment : CommentDesc =      {
                            # comment_id = 1,
                            "photo_id" : photo_id,
                            "display_author_name" : comment.display_author_name,
                            "display_photographer_name" : display_photographer_name,
                            "text" : comment.text }
                        
                        
                        my_comment = Comment(**dict(myComment) )
                        await my_comment.create()
                            
                        response.headers["Location"] = "/comment/" + display_photographer_name + "/"+ str(photo_id) +"/" + comment.display_author_name + "/" + str(my_comment.id)
                        # else:
                        #     raise HTTPException(status_code = 409, detail = "Conflict")
                    except pymongo.errors.ServerSelectionTimeoutError:
                        raise HTTPException(status_code=503, detail="Mongo unavailable")
                    
                elif photo.status_code == requests.codes.unavailable:
                    raise HTTPException(status_code = 503, detail = "Mongo unavailable")
                elif photo.status_code == requests.codes.not_found:
                    raise HTTPException(status_code = 404, detail = "Photo Not Found")
                    
            elif photographer.status_code == requests.codes.unavailable:
                raise HTTPException(status_code = 503, detail = "Mongo unavailable")
            elif photographer.status_code == requests.codes.not_found:
                raise HTTPException(status_code = 404, detail = "Photographer Not Found")
            
        elif author.status_code == requests.codes.unavailable:
            raise HTTPException(status_code = 503, detail = "Mongo unavailable")
        elif author.status_code == requests.codes.not_found:
            raise HTTPException(status_code = 404, detail = "Author Not Found")

       

@app.get("/comments/{display_photographer_name}/{photo_id}", response_model = Comments, status_code = 200, description=" get all comments of a specific photo")    
async def get_comments_on_photo(display_photographer_name: str = Photographer.PATH_PARAM, photo_id: int = PhotoId.PATH_PARAM, offset: int = 0, limit: int = 10):
                
        photographer = requests.get(photographer_service + 'photographer/' + display_photographer_name,  timeout=REQUEST_TIMEOUT)
        if photographer.status_code == requests.codes.ok:
        # if True:

            # on verifie l'existence de la photo
            photo = requests.get(photo_service + 'photo/' + display_photographer_name + "/" + str(photo_id) + "/attributes",  timeout=REQUEST_TIMEOUT)
            if photo.status_code == requests.codes.ok:
            # if True :
                # on enregistre le commentaire
                list_of_digests = list()
                last_id = 0

                try:

                    async for result in Comment.find(Comment.photo_id == photo_id,
                                            Comment.display_photographer_name == display_photographer_name
                                ).sort("_id").skip(offset).limit(limit):
                                
                                digest = CommentDigest(
                                    comment_id=result.id ,
                                    link= f"/comment/{display_photographer_name}/{photo_id}/{result.id}" )
                                
                                last_id = result.id
                                list_of_digests.append(digest)
                                
                except pymongo.errors.ServerSelectionTimeoutError:
                    raise HTTPException(status_code=503, detail="Mongo unavailable")

               
                has_more = await Comment.find(Comment.id > last_id).to_list()
                
                return {'items': list_of_digests, 'has_more': True if len(has_more) else False}


            elif photo.status_code == requests.codes.unavailable:
                raise HTTPException(status_code = 503, detail = "Mongo unavailable")
            elif photo.status_code == requests.codes.not_found:
                raise HTTPException(status_code = 404, detail = "Photo Not Found")
                
        elif photographer.status_code == requests.codes.unavailable:
            raise HTTPException(status_code = 503, detail = "Mongo unavailable")
        elif photographer.status_code == requests.codes.not_found:
            raise HTTPException(status_code = 404, detail = "Photographer Not Found")

@app.get("/comments/{display_photographer_name}/{photo_id}/{display_author_name}", response_model = Comments, status_code = 200, description=" get all comments of a author on a specific photo")    
async def get_comments_of_user( display_photographer_name: str = Photographer.PATH_PARAM,
                                photo_id: int = PhotoId.PATH_PARAM,
                                display_author_name: str = Author.PATH_PARAM,
                                offset: int = 0, limit: int = 10
                                ):
    author = requests.get(photographer_service + 'photographer/' + display_author_name,  timeout=REQUEST_TIMEOUT)
    if author.status_code == requests.codes.ok: 
    # if True:
        photographer = requests.get(photographer_service + 'photographer/' + display_photographer_name,  timeout=REQUEST_TIMEOUT)
        if photographer.status_code == requests.codes.ok:
        # if True:

            # on verifie l'existence de la photo
            photo = requests.get(photo_service + 'photo/' + display_photographer_name + "/" + str(photo_id) + "/attributes",  timeout=REQUEST_TIMEOUT)
            if photo.status_code == requests.codes.ok:
            # if True:
                # on enregistre le commentaire
                list_of_digests = list()
                last_id = 0
                try:
                    async for result in Comment.find(
                                            Comment.photo_id == photo_id,
                                            Comment.display_photographer_name == display_photographer_name,
                                            Comment.display_author_name == display_author_name
                                ).sort("_id").skip(offset).limit(limit):

                        digest = CommentDigest(
                            comment_id=result.id ,
                            link= f"/comment/{display_photographer_name}/{photo_id}/{result.id}" )
                        last_id = result.id
                        list_of_digests.append(digest)
                except pymongo.errors.ServerSelectionTimeoutError:
                    raise HTTPException(status_code=503, detail="Mongo unavailable")
                    
                has_more = await Comment.find(Comment.id > last_id).to_list()
                return {'items': list_of_digests, 'has_more': True if len(has_more) else False}


            elif photo.status_code == requests.codes.unavailable:
                raise HTTPException(status_code = 503, detail = "Mongo unavailable")
            elif photo.status_code == requests.codes.not_found:
                raise HTTPException(status_code = 404, detail = "Photo Not Found")
                
        elif photographer.status_code == requests.codes.unavailable:
            raise HTTPException(status_code = 503, detail = "Mongo unavailable")
        elif photographer.status_code == requests.codes.not_found:
            raise HTTPException(status_code = 404, detail = "Photographer Not Found")

    elif author.status_code == requests.codes.unavailable:
        raise HTTPException(status_code = 503, detail = "Mongo unavailable")
    elif author.status_code == requests.codes.not_found:
        raise HTTPException(status_code = 404, detail = "Author Not Found")
    

@app.get("/comment/{comment_id}", response_model = CommentDesc, status_code = 200, description=" get a comment made by an author on a specific photo")    
async def get_comment(comment_id: object =  TheCommentId.PATH_PARAM ):

        try:
            the_comment =  await Comment.get(comment_id)
            if the_comment is not None:
                return the_comment
            else:
                raise HTTPException(status_code = 404, detail = "The Comment does not exist") 
                
        except pymongo.errors.ServerSelectionTimeoutError:
            raise HTTPException(status_code=503, detail="Mongo unavailable")


@app.get("/count/comments/{display_photographer_name}/{photo_id}", status_code = 200, description=" get the number of comment on a specific photo")    
async def get_count_comments(display_photographer_name: str = Photographer.PATH_PARAM, photo_id: int = PhotoId.PATH_PARAM):

        photographer = requests.get(photographer_service + 'photographer/' + display_photographer_name,  timeout=REQUEST_TIMEOUT)
        if photographer.status_code == requests.codes.ok:
        # if True:

            # on verifie l'existence de la photo
            photo = requests.get(photo_service + 'photo/' + display_photographer_name + "/" + str(photo_id) + "/attributes",  timeout=REQUEST_TIMEOUT)
            if photo.status_code == requests.codes.ok:
            # if True :
  
                try:

                  
                    comments = await Comment.find_many(
                                        # Comment.photo_id == photo_id,
                                        Comment.display_photographer_name == display_photographer_name
                                            ).to_list()
                        
                    return {'number': len(comments)}
                                
                except pymongo.errors.ServerSelectionTimeoutError:
                    raise HTTPException(status_code=503, detail="Mongo unavailable")

            elif photo.status_code == requests.codes.unavailable:
                raise HTTPException(status_code = 503, detail = "Mongo unavailable")
            elif photo.status_code == requests.codes.not_found:
                raise HTTPException(status_code = 404, detail = "Photo Not Found")
                
        elif photographer.status_code == requests.codes.unavailable:
            raise HTTPException(status_code = 503, detail = "Mongo unavailable")
        elif photographer.status_code == requests.codes.not_found:
            raise HTTPException(status_code = 404, detail = "Photographer Not Found")


@app.put("/comment/{comment_id}", status_code = 200, description= " update a comment ")
async def update_comment(comment_id: object =  TheCommentId.PATH_PARAM, comment: CommentDesc = COMMENT_UPDATE_BODY):
        try:
            found =  await Comment.get(comment_id)
            found.text = comment.text
            if found is not None:
                await found.replace()
            else:
                raise HTTPException(status_code = 404, detail = "The Comment does not exist") 
                
        except pymongo.errors.ServerSelectionTimeoutError:
            raise HTTPException(status_code=503, detail="Mongo unavailable")


@app.delete("/comment/{comment_id}", status_code = 200, description=" delete a comment")
async def delete_comment(comment_id: object =  TheCommentId.PATH_PARAM):
            
        try:
            found =  await Comment.get(comment_id)
            if found is not None:
                await found.delete()
            else:
                raise HTTPException(status_code = 404, detail = "The Comment does not exist") 
                
        except pymongo.errors.ServerSelectionTimeoutError:
            raise HTTPException(status_code=503, detail="Mongo unavailable")


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, log_level="info")
    #logger.setLevel(logging.DEBUG)
else:
    #logger.setLevel(gunicorn_logger.level)
    pass