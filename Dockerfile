FROM python:3.10
WORKDIR "/app"
COPY ./requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt 
# COPY ./comment_service.py /app
# COPY ./comment_wrapper.py /app
# COPY ./comment_const.py /app
# COPY ./models.py /app
COPY ./*.py /app/
#EXPOSE 80

ENV MONGO_HOST mongo-service
ENV PHOTOGRAPHER_HOST photographer-service
ENV PHOTO_HOST photo-service

CMD ["uvicorn", "comment_service:app", "--host", "0.0.0.0", "--port", "80"]  

#  --host", "0.0.0.0" --port", "80"