import pytest
# from starlette.testclient import TestClient
import json
from bson import json_util
import logging
# from fastapi.testclient import TestClient
from comment_service import app
from beanie import Document, init_beanie
from httpx import AsyncClient, Request
#logging.basicConfig(level=logging.DEBUG)
import unittest.mock


## Avant de lancer les testi il faut créer  les photos  0 et 1 de AVEIRO ET DE ronaldo

data1 = {
    #"display_photographer_name" : "aveiro",
    #"photo_id" : "0",
    "display_author_name" : "alpha",
    "text" : "Bonjir"
    
}


data2 = {
    #"display_photographer_name" : "ronaldo",
    #"photo_id" : "1",
    "display_author_name" : "alpha",
    "text" : "buenos"
    
}
data_update = {
    "text" : "update comment"
    
}

headers_content = {'Content-Type': 'application/json'}
headers_accept  = {'Accept': 'application/json'}





@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_post_once(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post('/comment/ronaldo/0',
                               headers=headers_content,
                                 content=json.dumps(data1))
        assert response.headers['Location']
        assert response.status_code == 201

@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_post_twice(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))
        assert response1.status_code == 201

        response2 = await ac.post('/comment/ronaldo/1',
                                  headers=headers_content,
                                  content=json.dumps(data2))
        assert response2.status_code == 201


@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_has_more_false_comments(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response = await ac.post('/comment/ronaldo/0',
                           headers=headers_content,
                           content=json.dumps(data1))
        assert response.headers['Location']
        assert response.status_code == 201

        response2 = await ac.get('/comments/ronalo/0?offset=0&limit=10')
        assert response2.status_code == 200
        assert response2.json()['has_more'] == False

@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')        
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_has_more_true_comments(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))

        assert response1.headers['Location']
        assert response1.status_code == 201

        response2 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data2))
        assert response2.headers['Location']
        assert response2.status_code == 201

        response3 = await ac.get('/comments/aveiro/0?offset=0&limit=1')
        assert response3.status_code == 200
        assert response3.json()['has_more'] == True

#get comments of an user
@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')        
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_get2_has_more_true_comments(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))

        assert response1.headers['Location']
        assert response1.status_code == 201

        response2 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data2))
        assert response2.headers['Location']
        assert response2.status_code == 201

        response3 = await ac.post('/comment/aveiro/1',
                                  headers=headers_content,
                                  content=json.dumps(data2))
        assert response3.headers['Location']
        assert response3.status_code == 201


        response4 = await ac.get('/comments/aveiro/0/alpha?offset=0&limit=1')
        assert response4.status_code == 200
        assert response4.json()['has_more'] == True 

# get a specific comment
@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')        
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_get_comment(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))

        assert response1.headers['Location']
        assert response1.status_code == 201

        # response4 = await ac.post('/comment/aveiro/0',
        #                           headers=headers_content,
        #                           content=json.dumps(data2))

        # assert response4.headers['Location']
        # assert response4.status_code == 201

        response2=await ac.get('comments/aveiro/0/alpha?offet=0&limit=1')
        assert response2.status_code==200

        response2 = await ac.get('/comment/'+str(response2.json()["items"][0]['comment_id']),
                                  headers=headers_content,
                                 )
        assert response2.status_code == 200


        # response3 = await ac.get('/comments/aveiro/0/alpha?offset=0&limit=1')
        # assert response3.status_code == 200
        # assert response3.json()['has_more'] == True                


@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')        
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_update_comments(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))

        assert response1.headers['Location']
        assert response1.status_code == 201

        response4 = await ac.get('/comments/aveiro/0?offset=0&limit=1')
        assert response4.status_code == 200

        response2 = await ac.put('/comment/'+str(response4.json()["items"][0]['comment_id']),
                                  headers=headers_content,
                                  content=json.dumps(data_update))
        assert response2.status_code == 200

    
@pytest.mark.asyncio
@unittest.mock.patch('comment_service.requests.get')        
@pytest.mark.usefixtures("clearComments")
@pytest.mark.usefixtures("initDB")
async def test_delete_comments(requests_get):
    requests_get.return_value.status_code = 200
    async with AsyncClient(app=app, base_url="http://test") as ac:
        response1 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data1))

        assert response1.headers['Location']
        assert response1.status_code == 201


        response2 = await ac.post('/comment/aveiro/0',
                                  headers=headers_content,
                                  content=json.dumps(data2))
        assert response2.headers['Location']
        assert response2.status_code == 201

        response4 = await ac.get('/comments/aveiro/0?offset=0&limit=1')
        assert response4.status_code == 200

        response3 = await ac.delete('/comment/'+str(response4.json()["items"][0]['comment_id']))
        assert response3.status_code == 200

        response5 = await ac.get('/comments/aveiro/0?offset=0&limit=1')
        assert response5.status_code == 200
        assert response5.json()['has_more'] == False



# # get a specific comment
# @pytest.mark.asyncio
# @unittest.mock.patch('comment_service.requests.get')        
# @pytest.mark.usefixtures("clearComments")
# @pytest.mark.usefixtures("initDB")
# async def test_get_count_comments(requests_get):
#   requests_get.return_value.status_code = 200
#     async with AsyncClient(app=app, base_url="http://test") as ac:
#         response1 = await ac.post('/comment/aveiro/0',
#                                   headers=headers_content,
#                                   content=json.dumps(data1))

#         assert response1.headers['Location']
#         assert response1.status_code == 201

#         response4 = await ac.post('/comment/aveiro/0',
#                                   headers=headers_content,
#                                   content=json.dumps(data2))

#         assert response4.headers['Location']
#         assert response4.status_code == 201


#         response3 = await ac.get('/count/comments/aveiro/0')
#         assert response3.status_code == 200





