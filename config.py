from pydantic import BaseSettings

class Settings(BaseSettings):
    mongo_host: str = "mongo-service" # "localhost"
    mongo_port: str = "27017"
    mongo_user: str = ""
    mongo_password: str = ""
    database_name: str = "comments"
    auth_database_name: str = "photographers"


    photographer_host: str = "photographer-service"
    photographer_port: str = "80"

    photo_host: str = "photo-service"
    photo_port: str = "80"